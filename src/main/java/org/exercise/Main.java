package org.exercise;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException {
        Exercises exercises = new Exercises();
        List<Method> methodList = generateMethodList();

        methodList.stream()
                .sorted(Comparator.comparing(method -> method.getAnnotation(Priority.class).value()))
                .forEach(method -> {
                    System.out.println(method.getAnnotation(Priority.class).description());
                    method.setAccessible(true);
                    try {
                        method.invoke(exercises);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    private static List<Method> generateMethodList() throws NoSuchMethodException {
        Class<Exercises> exercisesClazz = Exercises.class;
        return List.of(
                exercisesClazz.getDeclaredMethod("firstLevel", null),
                exercisesClazz.getDeclaredMethod("secondLevel", null),
                exercisesClazz.getDeclaredMethod("thirdLevel", null),
                exercisesClazz.getDeclaredMethod("fourthLevel", null)
        );
    }
}