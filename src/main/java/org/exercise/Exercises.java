package org.exercise;

public class Exercises {

    @Priority(value = 4, description = "First level of exercises")
    public void firstLevel() {
        System.out.println("Do Crunches");
    }

    @Priority(value = 2, description = "Second level of exercises")
    protected void secondLevel() {
        System.out.println("Do Squats");
    }

    @Priority(value = 3, description = "Thrid level of exercises")
    private void thirdLevel() {
        System.out.println("Do Push-ups");
    }

    @Priority(value = 1, description = "Fourth level of exercises")
    void fourthLevel() {
        System.out.println("Do Pull-ups");
    }
}
